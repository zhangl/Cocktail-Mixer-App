﻿using System;

namespace corecrawler
{
    public class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }

        private readonly IWebCrawler _webCrawler;
        private readonly ICocktailDB _cocktailDB;

        public Program() : this(new WebCrawler(), new CocktailDB()) { }
        public Program(IWebCrawler crawler, ICocktailDB cocktailDB)
        {
            _webCrawler = crawler;
            _cocktailDB = cocktailDB;
        }

        public void Run()
        {
            var categories = _webCrawler.GetAllCategories().Result;
            foreach (var category in categories.drinks)
            {
                var recipes = _webCrawler.GetRecipeIdsForCategory(category.strCategory.ToString()).Result;
                foreach (var recipeId in recipes.drinks)
                {
                    var recipe = _webCrawler.GetRecipeDetails(recipeId.idDrink.ToString()).Result;
                    _cocktailDB.SaveRecipe(recipe);
                }
            }
        }
    }
}
