using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace corecrawler
{
    public class WebCrawler : IWebCrawler
    {
        public async Task<dynamic> GetAllIngredientsAsync()
        {
            return await GetData<dynamic>("list.php?i=list");
        }

        public async Task<dynamic> GetAllCategories()
        {
            return await GetData<dynamic>("list.php?c=list");
        }

        public async Task<dynamic> GetRecipeIdsForCategory(string category)
        {
            return await GetData<dynamic>($"filter.php?c={category}");
        }

        public async Task<dynamic> GetRecipeDetails(string recipeId)
        {
            var recipeDetails = await GetData<dynamic>($"lookup.php?i={recipeId}");
            var recipe = recipeDetails.drinks[0];
            var name = recipe.strDrink;
            var instructions = recipe.strInstructions;
            var ingredients = CleanArray(new string[] {
                recipe.strIngredient1, recipe.strIngredient2, recipe.strIngredient3, recipe.strIngredient4, recipe.strIngredient5,
                recipe.strIngredient6, recipe.strIngredient7, recipe.strIngredient8, recipe.strIngredient9, recipe.strIngredient10,
                recipe.strIngredient11, recipe.strIngredient12, recipe.strIngredient13, recipe.strIngredient14, recipe.strIngredient15
            });
            var measurements = CleanArray(new string[] {
                recipe.strMeasure1, recipe.strMeasure2, recipe.strMeasure3, recipe.strMeasure4, recipe.strMeasure5,
                recipe.strMeasure6, recipe.strMeasure7, recipe.strMeasure8, recipe.strMeasure9, recipe.strMeasure10,
                recipe.strMeasure11, recipe.strMeasure12, recipe.strMeasure13, recipe.strMeasure14, recipe.strMeasure15
            });
            return new {
                Name = name,
                Instructions = instructions,
                Ingredients = ingredients,
                Measurements = measurements
            };
        }

        private async Task<T> GetData<T>(string endpoint)
        {
            Thread.Sleep(100);
            using (var client = new HttpClient())
            {
                try 
                {
                    var response = await client.GetStringAsync("http://www.thecocktaildb.com/api/json/v1/1/" + endpoint);
                    return (dynamic)JObject.Parse(response);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return default(T);
                }
            }
        }

        private string[] CleanArray(string[] input)
        {
            return input.TakeWhile(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }
    }
}