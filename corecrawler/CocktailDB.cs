using System;
using Neo4j.Driver.V1;
using System.Text.RegularExpressions;

namespace corecrawler
{
    public class CocktailDB : ICocktailDB
    {
        public void SaveRecipe(dynamic recipe)
        {
            var cleanName = CleanString(recipe.Name.ToString());
            var cleanInstructions = CleanString(recipe.Instructions.ToString());

            using (var driver = GraphDatabase.Driver("bolt://hobby-jgkjbiaijildgbkepkicmfol.dbs.graphenedb.com:24786", AuthTokens.Basic("cocktailmixer", "b.RtrKMCRMbrJc.FfcYwVIydSBlgGij")))
            using(var session = driver.Session())
            {
                for (var index = 0; index < recipe.Ingredients.Length; index++)
                {
                    var cleanIngredient = CleanString(recipe.Ingredients[index].ToString());
                    var measurement = recipe.Measurements.Length >= index + 1 ? CleanString(recipe.Measurements[index].ToString().Trim()) : "";
                    var query = $"MERGE (r:Recipe {{name:'{cleanName}', instructions: '{cleanInstructions}'}}) MERGE(i:Ingredient {{name:'{cleanIngredient}'}}) MERGE (r)-[:CONTAINS {{amount:['{measurement}']}}]-(i)";
                    Console.WriteLine(query.ToString());
                    session.Run(query.ToString());
                }
            }
        }

        private string ToTitleCase(string input)
        {
            var tokens = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < tokens.Length; i++)
            {
                var token = tokens[i];
                tokens[i] = token.Substring(0, 1).ToUpper() + token.Substring(1).ToLower();
            }

            return string.Join(" ", tokens);
        }

        private string CleanString(string input)
        {
            // input = input.Replace("'", "");
            //input = input.Replace(".", "");
            //input = input.Replace("/", "");
            //input = input.Replace("#", "");
            //input = input.Replace("&", "");
            //input = input.Replace("(", "");
            //input = input.Replace(")", "");
            //input = input.Replace("-", " ");
            // return input;
            var pattern = @"[\\']+";
            return Regex.Replace(input, pattern, @"\$&"); //TODO: Learn RegEx
        }
    }
}