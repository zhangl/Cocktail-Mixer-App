using System.Threading.Tasks;

namespace corecrawler
{
    public interface IWebCrawler
    {
        Task<dynamic> GetAllIngredientsAsync();
        Task<dynamic> GetAllCategories();
        Task<dynamic> GetRecipeIdsForCategory(string category);
        Task<dynamic> GetRecipeDetails(string recipeId);
    }
}