namespace corecrawler
{
    public interface ICocktailDB
    {
        void SaveRecipe(dynamic recipe);
    }
}