var neo4j = require('neo4j');
//var db = new neo4j.GraphDatabase('http://neo4j:bartender@localhost:7474');
var db = new neo4j.GraphDatabase('http://cocktailmixer:b.RtrKMCRMbrJc.FfcYwVIydSBlgGij@hobby-jgkjbiaijildgbkepkicmfol.dbs.graphenedb.com:24789/db/data/');

function cleanIdentity(input) {
    return input.replace(/[^A-Za-z]/g, '');
};

function cleanName(input) {
    return input.replace('\'', '');
};

exports.saveRecipe = function(identity, name, ingredients, measurements) {
    try {
        db.cypher({
            query: 'CREATE (' + cleanIdentity(identity) + ':Recipe {name: \'' + cleanName(name) + '\'})'
        }, function (err) {
            if (err) {
                console.log('identity: ' + identity);
                console.log('name: ' + name);
                console.error(err);
            }
        });
        for (var index = 0; index < ingredients.length; index++) {
            try {
                var measurement = measurements.length - 1 >= index ? measurements[index] : "";
                db.cypher({
                    query: 'CREATE (' + cleanIdentity(identity) + ')-[:CONTAINS {measurement:[\'' + cleanName(measurement) + '\']}]->(' + cleanIdentity(ingredients[index]) + ')'
                }, function (err) {
                    if (err) {
                        console.log('identity: ' + identity);
                        console.log('index: ' + index);
                        console.log('measurement: ' + measurements[index]);
                        console.log('ingredient: ' + ingredients[index]);
                        console.error(err);
                    }
                });
            } catch (ex) {
                console.log('identity: ' + identity);
                console.log('index: ' + index);
                console.log('measurement: ' + measurements[index]);
                console.log('ingredient: ' + ingredients[index]);
                console.error(ex);
            }
        }
    } catch (ex) {
        console.log('identity: ' + identity);
        console.log('name: ' + name);
        console.error(ex);
    }
};

exports.saveIngredient = function(identity, name) {
    var identity = cleanIdentity(identity);
    db.cypher({
        query: 'CREATE (' + cleanIdentity(identity) + ':Ingredient {name: \'' + cleanName(name) + '\'})'
    }, function (err, results) {
        if (err) {
            console.log('identity: ' + identity);
            console.log('name: ' + name);
            console.error(err);
        }
    });
};