var crawler = require('./crawler');
var mixerdb = require('./cocktailmixerdb');


function sleep(ms, id) {
  return new Promise(resolve => setTimeout(resolve(id), ms));
}

function loadAllIngredients() {
    crawler.getAllIngredients(function(data) {
        data.forEach(function(name){
            mixerdb.saveIngredient(name, name);
        });
    })
};

function loadAllRecipes() {
    crawler.getAllCategories(function(categories) {
        categories.forEach(function(category) {
            crawler.getAllRecipeIdsInCategory(category, function(recipeIds) {
                for (var index = 0; index < recipeIds.length; index++) {
                    var recipeId = recipeIds[index];
                    sleep(2000, recipeId).then((id) => {
                        crawler.getRecipeDetails(id, function(recipeDetails) {
                            mixerdb.saveRecipe(
                                recipeDetails.name,
                                recipeDetails.name,
                                recipeDetails.ingredients,
                                recipeDetails.measurements);
                        });
                    });
                }
            });
        })
    });
};

//loadAllIngredients();
loadAllRecipes();