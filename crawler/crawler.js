var http = require('http');

function cleanArray(input) {
    var output = [];
    for (var index = 0; index < input.length; index++) {
        if (input[index] !== null && input[index].trim() !== '') {
            output.push(input[index]);
        }
    }
    return output;
};

function getData(endpoint, callback) {
    console.log(endpoint);
    var request = http.get('http://www.thecocktaildb.com/api/json/v1/1/' + endpoint, function(response) {
        response.setEncoding('utf-8');

        var responseString = '';
        response.on('data', function(data) {
            responseString += data;
        });

        response.on('end', function() {
            callback(JSON.parse(responseString));
        });
    });
}

exports.getAllIngredients = function(callback) {
    getData('list.php?i=list', function(data) {
        var ingredients = [];
        data.drinks.forEach(function(ingredient){
            var name = ingredient.strIngredient1;
            ingredients.push(name);
        });
        callback(ingredients);
    });
};

exports.getAllCategories = function(callback) {
    getData('list.php?c=list', function(data) {
        var categories = [];
        data.drinks.forEach(function(category){
            var name = category.strCategory;
            categories.push(name);
        });
        callback(categories);
    })
};

exports.getAllRecipeIdsInCategory = function(categoryName, callback) {
    getData('filter.php?c=' + categoryName, function(data) {
        var recipeIds = [];
        data.drinks.forEach(function(recipe) {
            recipeIds.push(recipe.idDrink);
        });
        callback(recipeIds);
    });
};

exports.getRecipeDetails = function(recipeId, callback) {
    getData('lookup.php?i=' + recipeId, function(data) {
        var recipe = data.drinks[0];
        var name = recipe.strDrink;
        var instructions = recipe.strDrink;
        
        var ingredients = cleanArray([recipe.strIngredient1, recipe.strIngredient2, recipe.strIngredient3, recipe.strIngredient4, recipe.strIngredient5,
            recipe.strIngredient6, recipe.strIngredient7, recipe.strIngredient8, recipe.strIngredient9, recipe.strIngredient10,
            recipe.strIngredient11, recipe.strIngredient12, recipe.strIngredient13, recipe.strIngredient14, recipe.strIngredient15]);
        var measurements = cleanArray([recipe.strMeasure1, recipe.strMeasure2, recipe.strMeasure3, recipe.strMeasure4, recipe.strMeasure5,
            recipe.strMeasure6, recipe.strMeasure7, recipe.strMeasure8, recipe.strMeasure9, recipe.strMeasure10,
            recipe.strMeasure11, recipe.strMeasure12, recipe.strMeasure13, recipe.strMeasure14, recipe.strMeasure15]);

        callback({
            name: recipe.strDrink,
            instructions: recipe.strDrink,
            ingredients: ingredients,
            measurements: measurements
        });
    });
};