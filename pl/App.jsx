import React from 'react';
import { ButtonToolbar, Button, Form, FormGroup, FormControl, Tabs, Tab } from 'react-bootstrap';
import Autocomplete from 'react-autocomplete';
import RecipeList from './RecipeList.jsx';
import IngredientsTab from './IngredientsTab.jsx';

export let styles = {
  item: {
    padding: '2px 6px',
    cursor: 'default'
  },

  highlightedItem: {
    color: 'white',
    background: 'hsl(200, 50%, 50%)',
    padding: '2px 6px',
    cursor: 'default'
  },

  menu: {
    border: 'solid 1px #ccc'
  }
}

export let sortByName = (list) => {
    return list.sort(function(a, b) {
        var nameA = a.name.toUpperCase();
        var nameB = b.name.toUpperCase();
        if (nameA < nameB)
            return -1;
        if (nameA > nameB)
            return 1;
        return 0;
    });
}

class App extends React.Component {

    render() {
        return (
            <div>
                <Header/>
                <TopTabs/> 
            </div>
        );
    }
}

class Header extends React.Component {
   render() {
      return (
         <div>
            <h1>Cocktail Mixer</h1>
         </div>
      );
   }
}

class TopTabs extends React.Component {
    constructor(props) {
        super();
        this.state = {
            // Takes active tab from props if it is defined there
            activeTab: props.activeTab || 1
        };

        // Bind the handleTabSelect function already here (not in the render function)
        this.handleTabSelect = this.handleTabSelect.bind(this);
    }
  
    handleTabSelect(selectedTab) {
        // The active tab must be set into the state so that
        // the Tabs component knows about the change and re-renders.
        this.setState({
            activeTab: selectedTab
        });
    }

    render() {
        return (
            <Tabs defaultActiveKey={1} activeKey={this.state.activeTab} onSelect={this.handleTabSelect} id="cocktail-tabs">
                <Tab eventKey={1} title="Ingredients" style={{paddingTop: 10}}><IngredientsTab handleTabSelect={this.handleTabSelect.bind(this)}/></Tab>
                <Tab eventKey={2} title="Recipes" style={{paddingTop: 10}}><RecipeList/></Tab>
            </Tabs>
        );
    }
}

export default App;