import React from 'react';
import { Accordion, Panel } from 'react-bootstrap';
import { sortByName } from './App.jsx';
import { selectedIngredients, recipes } from './IngredientsTab.jsx';

class RecipeList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            ingredients: this.props.ingredients
        }
    }  

    render() {
        let recipesList = null;

        if(selectedIngredients.length > 0){
            recipesList = <Accordion>
                {recipes.map(recipe => (
                    <Panel header={recipe.name} key={recipe.id} eventKey={recipe.id}>
                        {recipe.ingredients.map((ingredient, index) => (
                            <div key={index} >{ingredient.amount} - {ingredient.name}</div>))
                        }
                    </Panel>          
                    ))
                }
            </Accordion>;
        }else{
            recipesList = <h4>No recipes found</h4>
        }

        return (
            <div>{recipesList}</div>
        );
    }
}

export default RecipeList;