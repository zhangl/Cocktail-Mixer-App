import React from 'react';
import { ButtonToolbar, Button, Form, FormGroup, FormControl } from 'react-bootstrap';
import Autocomplete from 'react-autocomplete';
import { sortByName, styles } from './App.jsx';

export let selectedIngredients = [];
export let recipes = [];

var findRecipes = function(){
    var baseGetRecipesEndpoint = "https://cocktailbuilder.herokuapp.com/recipe/?";
    var ingredientIds = selectedIngredients.map(ingredient => {
        return "ingredientId=" + ingredient.id;
    });
    var queryString = ingredientIds.join("&");
    var getRecipesUrl = baseGetRecipesEndpoint + queryString;

    fetch(getRecipesUrl)
        .then((response) => response.json()) 
        .then((responseJSON) => {
            recipes = responseJSON;
        });
}

class IngredientsTab extends React.Component {
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClearAll = this.handleClearAll.bind(this);
        this.state = {filteredIngredients: [], allIngredients: [], selectedIngredients: [], selectedIngredient:"", recipes: []}

        fetch("https://cocktailbuilder.herokuapp.com/ingredients")
            .then((response) => response.json()) 
            .then((ingredients) => {
                ingredients = sortByName(ingredients);
                this.setState({
                    allIngredients: ingredients,
                    filteredIngredients: ingredients});
                });
    }

    render() {
        return (
            <div>
                <Form inline onSubmit={this.handleSubmit}>
                    <FormGroup style={{display: 'inline-flex'}}>
                        <Autocomplete
                            value={this.state.selectedIngredient.name}
                            inputProps={{ placeholder: 'Enter Ingredient', className: "form-control" }}
                            items={this.state.filteredIngredients}
                            getItemValue={(item) => item.name}
                            onSelect={(value, item) => {
                                this.setState({ selectedIngredient: item })
                            }}
                            onChange={(event, value) => {
                                this.setState({
                                    filteredIngredients: this.state.allIngredients.filter((_) => _.name.toLowerCase().includes(value.toLowerCase())),
                                    selectedIngredient: { name: value },
                                    loading: true })
                            }}
                            renderItem={(item, isHighlighted) => (
                                <div style={isHighlighted ? styles.highlightedItem : styles.item}
                                    key={item.id}
                                    id={item.id}
                                    >{item.name}
                                </div>
                            )}
                        />
                        <Button type="submit" bsStyle="primary" style={{marginLeft: 5}}>{"Add"}</Button>
                    </FormGroup>
                </Form> 
                <IngredientList onChange={this.handleDelete.bind(this)} style={{paddingTop: 10}}/>
                <ButtonToolbar style={{marginTop: 15}}>
                    <Button onClick={this.handleClearAll}>{"Clear All"}</Button>
                    <Button onClick={this.handleTabSelect.bind(this, 2)} bsStyle="primary">{"Find Recipes"}</Button>
                </ButtonToolbar>
            </div>
        );
    }

    handleTabSelect(tabToGoTo, e) {
        e.preventDefault();
        if (typeof this.props.handleTabSelect === 'function') {
            if(tabToGoTo === 2)
                findRecipes();

            this.props.handleTabSelect(tabToGoTo);
        }
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState(() => {
            selectedIngredients = selectedIngredients.concat(this.state.selectedIngredient);
        });

        this.setState({selectedIngredient: {}});
    }

    handleClearAll(e){
        e.preventDefault();
        this.setState(() => {
            selectedIngredients = [];
        })
    }

    handleDelete(index) {
        this.setState(() => {
            selectedIngredients = selectedIngredients.filter((_, i) => i !== index);
        })
    }
}

class IngredientList extends React.Component {
    changeHandler(index, e) {
        if (typeof this.props.onChange === 'function') {
            this.props.onChange(index);
        }
    }
    
    render() {
        return (
            <table>
                <tbody>
                {selectedIngredients.map((item, index) => (
                    <tr key={item.id}>
                        <td style={{paddingLeft: 10}}>
                            <i className="fa fa-2x fa-trash" style={{paddingRight: 5}} onClick={this.changeHandler.bind(this, index)}></i>{item.name}
                        </td>
                    </tr>           
                ))}
                </tbody>
            </table>
        );
    }
}

export default IngredientsTab;